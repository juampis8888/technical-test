using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowInfo : MonoBehaviour
{
    public RectTransform Content;

    public InfoAdapter InfoAdapter;

    private ManagerGuns ManagerGuns;

    // Start is called before the first frame update
    void Start()
    {
        ManagerGuns = gameObject.GetComponent<ManagerGuns>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Info(string tag)
    {
        DeleteChild();
        var ObjGun = ManagerGuns.SearchGun(tag);
        var ObjInfo = Instantiate(InfoAdapter);

        ObjInfo.Parent(Content);
        ObjInfo.NameGun.text = string.Format("{0}", ObjGun.NameGun);
        ObjInfo.SpriteGun.sprite = ObjGun.ImageGun;
        ObjInfo.TotalCapacityGun.text = string.Format("{0} {1}", ObjInfo.TotalCapacityGun.text, ObjGun.TotalCapacityGun.ToString());
        ObjInfo.GunCartridge.text = string.Format("{0} {1}", ObjInfo.GunCartridge.text, ObjGun.GunCartridge.ToString());
        ObjInfo.Damage.text = string.Format("{0} {1}", ObjInfo.Damage.text, ObjGun.Damage.ToString());
        ObjInfo.Description.text = string.Format("{0} {1}", ObjInfo.Description.text, ObjGun.DescriptionGun.ToString());
        ObjInfo.ButtonSelect.onClick.AddListener(() => 
        { 
            Select(tag);
        });
        ObjInfo.ButtonExit.onClick.AddListener(() =>
        {
            ExitInfo();
        });

        ObjInfo.transform.localPosition = new Vector3(0, 0, 0);
        ObjInfo.transform.localScale = Vector3.one;


    }

    public void Select(string tag)
    {   
        if(ManagerGuns.Player.transform.childCount > 2)
        {
            Destroy(ManagerGuns.Player.transform.GetChild(0).gameObject);
        }
        ExitInfo();
        var ObjGun = Instantiate(ManagerGuns.SearchGun(tag).PrefabGun);
        ObjGun.transform.SetParent(ManagerGuns.Player.transform);
        ObjGun.transform.localPosition = new Vector3(0,ObjGun.transform.position.y, ObjGun.transform.position.z);
        ObjGun.transform.localRotation = ObjGun.transform.rotation;
        ObjGun.GetComponent<MouseClick>().enabled = false;
        ObjGun.transform.SetAsFirstSibling();
        ManagerGuns.GunCurrent = tag;
        ManagerGuns.SetGun(ObjGun);
    }

    public void ExitInfo()
    {
        OnActive(false);
        DeleteChild();
    }

    public void DeleteChild()
    {   
        
        if (Content.childCount > 0)
        {
            Destroy(Content.GetChild(0).gameObject);
        }
    }

    public void OnActive(bool isActive)
    {
        Content.GetChild(0).gameObject.SetActive(isActive);
    }
}
