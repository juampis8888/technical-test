using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    private Animator AnimatorPJ;

    private GameManager GameManager;

    private SelectAnimation SelectAnimation;

    private void Awake()
    {
        AnimatorPJ = gameObject.GetComponent<Animator>();
        GameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        SelectAnimation = GameObject.FindGameObjectWithTag("GameManager").GetComponent<SelectAnimation>();
    }

    private void Update()
    {
        if (GameManager.GetIndexScene() == 1)
        {
            ActivarAnimacion(GameManager.AnimationCurrent);
        }
    }

    public void ActivarAnimacion(string NameState)
    {
        
        if (GameManager.GetIndexScene() == 0)
        {
            AnimatorPJ.Play(NameState,0,0);
            GameManager.AnimationCurrent = NameState;
            SelectAnimation.SetTextInfo(string.Format("Selected Animation: {0}", NameState));
            SelectAnimation.ActiveInteractable();
            SelectAnimation.Select(string.Format("Accept {0}", NameState));
        }
        else if(GameManager.GetIndexScene() == 1)
        {
            AnimatorPJ.Play(NameState);
        }
    }

    public void CallChangeScene()
    {
        if (GameManager.GetIndexScene() == 0 & SelectAnimation.GetIsSelectAnimation())
        {
            GameManager.ChangeScene();
        }
    }
}
