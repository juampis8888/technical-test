using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public string AnimationCurrent;

    public static GameManager Instance;

    public AnimationManager AnimationManager;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        AnimationManager = GameObject.FindGameObjectWithTag("Player").GetComponent<AnimationManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(1);
    }

    public int GetIndexScene()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }
}
