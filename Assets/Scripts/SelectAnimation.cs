using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectAnimation : MonoBehaviour
{   
    public TextMeshProUGUI TextSelectAnimation;

    public TextMeshProUGUI TextInfo;

    public Button ButtonSelect;

    private GameManager gameManager;

    private bool IsSelectAnimation = false;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        ButtonSelect.onClick.AddListener(() =>
        {
            gameManager.AnimationManager.ActivarAnimacion(gameManager.AnimationCurrent);        
        });
    }

    public void Select(string value)
    {
        TextSelectAnimation.text = value;
    }

    public void SetTextInfo(string value)
    {
        TextInfo.text = value;
    }

    public void ActiveSelect()
    {
        IsSelectAnimation = true;
    }

    public bool GetIsSelectAnimation()
    {
        return IsSelectAnimation;
    }

    public void ActiveInteractable()
    {
        ButtonSelect.interactable = true;
    }
}
